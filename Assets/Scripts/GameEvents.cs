﻿using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

namespace WildMatch
{
    public sealed class GameEvents : MonoBehaviour
    {
        sealed class Event : UnityEvent<object, object> { }

        static GameEvents Manager
        {
            get
            {
                if (Instance) return Instance;

                Instance = FindObjectOfType<GameEvents>();

                if (!Instance) Debug.LogError("GameEvents not found.");

                return Instance;
            }
        }

        Dictionary<string, Event> _events = new Dictionary<string, Event>();

        static GameEvents Instance;

        public static void AddListener(string eventName, UnityAction<object, object> action)
        {
            var events = Manager._events;

            events.TryGetValue(eventName, out var gameEvent);

            if (gameEvent == null)
            {
                gameEvent = new Event();
                events[eventName] = gameEvent;
            }

            gameEvent.AddListener(action);
        }

        public static void RemoveListener(string eventName, UnityAction<object, object> action)
        {
            var events = Manager._events;

            events.TryGetValue(eventName, out var gameEvent);

            if (gameEvent == null) return;

            gameEvent.RemoveListener(action);
        }

        public static void TriggerEvent(string eventName, object sender, object eventArgs)
        {
            var events = Manager._events;

            events.TryGetValue(eventName, out var gameEvent);

            if (gameEvent == null) return;

            gameEvent.Invoke(sender, eventArgs);
        }
    }
}
