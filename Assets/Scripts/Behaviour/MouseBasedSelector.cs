﻿using UnityEngine;

namespace WildMatch.Behaviour
{
    [RequireComponent(typeof(Camera))]
    public sealed class MouseBasedSelector : MonoBehaviour
    {
        Camera _camera;

        void Start()
        {
            _camera = GetComponent<Camera>();
        }

        void Update()
        {
            if (!Input.GetMouseButtonDown(0)) return;
            
            var ray = _camera.ScreenPointToRay(Input.mousePosition);

            if (!Physics.Raycast(ray, out var hit, 1000)) return;

            var select = hit.collider.GetComponent<ISelectable>();

            if (select == null) return;

            select.IsSelected = !select.IsSelected;
        }
    }
}
