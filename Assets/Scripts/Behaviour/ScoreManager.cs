﻿using System.Collections.Generic;
using UnityEngine;

namespace WildMatch.Behaviour
{
    public sealed class ScoreManager : MonoBehaviour
    {
        int _currentScore;

        void OnEnable()
        {
            GameEvents.AddListener(EventName.MatchesFound, OnMatchesFound);
        }

        void OnMatchesFound(object sender, object eventArgs)
        {
            if (!enabled) return;

            var matchLines = (IEnumerable<Int2Line>)eventArgs;

            foreach(var line in matchLines)
            {
                var score = 10 + Mathf.Max(line.PointCount - 3, 0) * 5;
                _currentScore += score;
            }

            GameEvents.TriggerEvent(EventName.ScoreChanged, this, _currentScore);
        }
    }
}
