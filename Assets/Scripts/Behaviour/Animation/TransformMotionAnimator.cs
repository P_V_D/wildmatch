﻿using WildMatch.Animation;
using UnityEngine;

namespace WildMatch.Behaviour.Animation
{
    public sealed class TransformMotionAnimator : AnimatorBehaviour, IMotionAnimator
    {
        [SerializeField] float _defaultSpeed = 5;

        Vector3 _endPosition;

        IAnimationHandle IMotionAnimator.AnimateMovement(Vector3 targetPosition)
        {
            _endPosition = targetPosition;

            return Activate();
        }

        IAnimationHandle IMotionAnimator.AnimateMovement(Vector3 startPosition, Vector3 endPosition)
        {
            transform.position = startPosition;

            _endPosition = endPosition;

            return Activate();
        }

        void Update()
        {
            var current = transform.position;

            if (current != _endPosition && _defaultSpeed > 0)
            {
                current = Vector3.MoveTowards(current, _endPosition, Time.deltaTime * _defaultSpeed);

                transform.position = current;

                return;
            }

            enabled = false;
        }
    }
}
