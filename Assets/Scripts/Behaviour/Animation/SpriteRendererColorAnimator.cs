﻿using WildMatch.Animation;
using UnityEngine;

namespace WildMatch.Behaviour.Animation
{
    public sealed class SpriteRendererColorAnimator : AnimatorBehaviour, IColorAnimator
    {
        [SerializeField] SpriteRenderer _renderer = null;
        [SerializeField] float _defaultDuration   = 1;

        float _timeLeft;
        Color _startColor;
        Color _endColor;

        IAnimationHandle IColorAnimator.AnimateColor(Color target)
        {
            _timeLeft   = _defaultDuration;
            _startColor = _renderer.color;
            _endColor   = target;

            return Activate();
        }

        IAnimationHandle IColorAnimator.AnimateColor(Color start, Color final)
        {
            _timeLeft   = _defaultDuration;
            _startColor = start;
            _endColor   = final;

            return Activate();
        }   

        void Update()
        {
            _timeLeft -= Time.deltaTime;

            if (_timeLeft > 0)
            {
                _renderer.color = Color.Lerp(_startColor, _endColor, 1 - _timeLeft / _defaultDuration);

                return;
            }

            enabled = false;
        }
    }
}
