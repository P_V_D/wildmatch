﻿using WildMatch.Animation;
using UnityEngine;

namespace WildMatch.Behaviour.Animation
{
    public abstract class AnimatorBehaviour : MonoBehaviour
    {
        private sealed class Handle : IAnimationHandle
        {
            AnimatorBehaviour _animator;
            int _token;

            public Handle(AnimatorBehaviour animator, int token)
            {
                _animator = animator;
                _token = token;
            }

            bool IAnimationHandle.IsPlaying
            {
                get
                {
                    if (_animator == null) return false;
                    if (_token != _animator._token) return false;

                    return _animator.enabled;
                }
            }

            void IAnimationHandle.Finish()
            {
                if (_animator == null) return;
                if (_token != _animator._token) return;
                if (!_animator.enabled) return;

                _animator.enabled = false;
            }
        }

        int _token;

        protected IAnimationHandle Activate()
        {
            _token++;

            enabled = true;

            return new Handle(this, _token);
        }

        void Awake()
        {
            enabled = false;
        }
    }
}
