﻿using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System;

namespace WildMatch.Behaviour
{
    partial class Player
    {
        IEnumerator SwapCellsRoutine(ICellPrefab cellA, ICellPrefab cellB)
        {
            var indexA = GetIndex(cellA);
            var indexB = GetIndex(cellB);

            if (!AreNeighbours(indexA, indexB))
            {
                cellB.IsSelected = true;
                yield break;
            }

            GameEvents.TriggerEvent(EventName.GameRoutineStarted, this, null);

            yield return SwapCells(indexA, indexB);

            if (DetectMatches())
            {
                yield return ProcessFoundMatches();
            }
            else
            {                
                yield return SwapCells(indexA, indexB);
            }
           
            GameEvents.TriggerEvent(EventName.GameRoutineFinished, this, null);
        }

        IEnumerator ProcessFoundMatches()
        {
            do
            {
                GameEvents.TriggerEvent(EventName.MatchesFound, this, _matchedLines as IEnumerable<Int2Line>);

                var points = from line in _matchedLines from p in line.Points select p;
                points = points.Distinct();

                foreach (var p in points)
                {
                    _spawner.Despawn(_map[p.x, p.y]);

                    _map[p.x, p.y] = null;
                }

                _shifter.FirstToLastDim0(_map);

                for(var x = 0; x < _grid.Width; x++)
                    for(int q = -1, y = _grid.Height - 1; y >= 0; y--)
                    {
                        if (_map[x, y] != null)
                        {
                            PlayMovement(_map[x, y], x, y);
                            continue;
                        }

                        var fab = _spawner.Spawn(CellSpawnContext.CommonCell);

                        _map[x, y] = fab;

                        var start = _grid.Transform(x, q);
                        var final = _grid.Transform(x, y);

                        var handle = fab.Motion.AnimateMovement(start, final);

                        _handles.Add(handle); q--;
                    }        

                yield return WaitForAnimation();

            } while (DetectMatches());

            if (!DetectMoves()) yield return RerollMap();
        }

        bool AreNeighbours(Int2 indexA, Int2 indexB)
        {
            var dX = Math.Abs(indexA.x - indexB.x);
            var dY = Math.Abs(indexA.y - indexB.y);
            return dX + dY == 1;
        }

        IEnumerator SwapCells(Int2 indexA, Int2 indexB)
        {
            var cellA = _map[indexA.x, indexA.y];
            var cellB = _map[indexB.x, indexB.y];

            _map[indexA.x, indexA.y] = cellB;
            _map[indexB.x, indexB.y] = cellA;

            PlayMovement(cellA, indexB.x, indexB.y);
            PlayMovement(cellB, indexA.x, indexA.y);

            yield return WaitForAnimation();
        }

        Int2 GetIndex(ICellPrefab cell)
        {
            foreach (var (x, y, fab) in IterateMap())
                if (fab == cell) return (x, y);

            throw new InvalidProgramException("Cell was not found in the map");
        }
    }
}
