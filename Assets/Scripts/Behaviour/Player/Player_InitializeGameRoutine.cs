﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;

namespace WildMatch.Behaviour
{
    partial class Player
    {
        IEnumerator InitializeGameRoutine()
        {
            GameEvents.TriggerEvent(EventName.GameRoutineStarted, this, null);

            yield return SpawnCellsFirstTime();

            yield return RerollMap();

            GameEvents.TriggerEvent(EventName.GameRoutineFinished, this, null);
        }

        IEnumerator RerollMap()
        {
            GameEvents.TriggerEvent(EventName.RerollStarted, this, null);

            // Start shaking animation.

            while (true)
            {
                Shuffle(false);

                if (!DetectMatches() && DetectMoves()) break;

                yield return null;
            }           

            yield return new WaitForSeconds(1);

            GameEvents.TriggerEvent(EventName.RerollFinished, this, null);

            yield return UpdateCellPositions();
        }

        IEnumerator UpdateCellPositions()
        {
            foreach (var (i, j, fab) in IterateMap())
                PlayMovement(fab, i, j);

            yield return WaitForAnimation();
        }

        IEnumerator SpawnCellsFirstTime()
        {
            _map = new ICellPrefab[_grid.Width, _grid.Height];

            var generator = Generator(_grid.Width * _grid.Height);

            foreach(var (i, j, _) in IterateMap())
            {
                generator.MoveNext();

                _map[i, j] = generator.Current;
            }

            Shuffle(true);

            foreach (var (i, j, fab) in IterateMap())
                PlaySpawn(fab, i, j);

            yield return WaitForAnimation();
        }

        IEnumerator<ICellPrefab> Generator(int amount)
        {
            _gapCount = Mathf.Min(amount, _gapCount);

            for (var i = 0; i < _gapCount; i++)
                yield return _spawner.Spawn(CellSpawnContext.GapCell);

            for (var i = _gapCount; i < amount; i++)
                yield return _spawner.Spawn(CellSpawnContext.CommonCell);
        }

        void PlaySpawn(ICellPrefab target, int x, int y)
        {
            var startPos = _grid.Transform(x, -_grid.Height + y);

            var endPos = _grid.Transform(x, y);

            var handle = target.Motion.AnimateMovement(startPos, endPos);

            _handles.Add(handle);
        }

        void PlayMovement(ICellPrefab target, int x, int y)
        {
            var pos = _grid.Transform(x, y);

            var handle = target.Motion.AnimateMovement(pos);

            _handles.Add(handle);
        }
    }
}

