﻿using System.Collections.Generic;
using WildMatch.Algorythmics;
using WildMatch.Scriptable;
using WildMatch.Animation;
using System.Collections;
using UnityEngine;
using System.Linq;

namespace WildMatch.Behaviour
{
    public sealed partial class Player : MonoBehaviour
    {
        // TODO: Should be interface.
        [SerializeField] CellSpawner _spawner = null;
        [SerializeField] int _gapCount      = 3;
        [SerializeField] int _matchLineSize = 3;

        ICellPrefab[,] _map;
        IGameGrid _grid;
        YieldLock _lock;
        List<IAnimationHandle> _handles;
        List<Int2Line> _matchedLines;
        List<Int2Line> _foundMoves;

        FisherYatesSelectiveShuffle2D<ICellPrefab> _shuffler;
        StraightLineRecognizer2D<ICellPrefab> _lineRecognizer;
        StraightLinePosibilityPredictor2D<ICellPrefab> _movementPredictor;
        SelectiveShift2D<ICellPrefab> _shifter;

        void Awake()
        {
            _grid = GetComponent<IGameGrid>();
            _handles = new List<IAnimationHandle>();
            _lock = new YieldLock(this);
            _matchedLines = new List<Int2Line>();
            _foundMoves = new List<Int2Line>();

            _shuffler = new FisherYatesSelectiveShuffle2D<ICellPrefab>()
            {
                CanMovePredicate = x => !x.IsGap
            };

            _shifter = new SelectiveShift2D<ICellPrefab>()
            {
                CanMovePredicate = x => x != null && !x.IsGap,
            };

            _lineRecognizer = new StraightLineRecognizer2D<ICellPrefab>()
            {
                MinimumLineSize = _matchLineSize,
                ContinueLinePredicate = (a, b) =>
                {
                    if (a.IsGap) return false;
                    if (b.IsGap) return false;

                    return a.MatchID == b.MatchID;
                }
            };

            _movementPredictor = new StraightLinePosibilityPredictor2D<ICellPrefab>()
            {
                MinimumLineSize = _matchLineSize,
                CanMovePredicate = x => !x.IsGap,
                ContinueLinePredicate = (a, b) =>
                {
                    if (a.IsGap) return false;
                    if (b.IsGap) return false;

                    return a.MatchID == b.MatchID;
                }
            };
        }

        void Start()
        {
            _lock.StartCoroutine(() => InitializeGameRoutine());
        }

        void OnEnable()
        {
            GameEvents.AddListener(EventName.TwoCellsSelected, OnTwoCellsSelected);
        }

        void OnDisable()
        {
            GameEvents.RemoveListener(EventName.TwoCellsSelected, OnTwoCellsSelected);
        }

        void OnTwoCellsSelected(object sender, object args)
        {
            var cells = ((ICellPrefab, ICellPrefab))args;

            _lock.StartCoroutine(() => SwapCellsRoutine(cells.Item1, cells.Item2));
        }

        IEnumerable<(int i, int j, ICellPrefab prefab)> IterateMap()
        {
            for (var i = 0; i < _map.GetLength(0); i++)
                for (var j = 0; j < _map.GetLength(1); j++)
                    yield return (i, j, _map[i, j]);
        }

        IEnumerator WaitForAnimation()
        {
            yield return new WaitWhile(() => _handles.Any(x => x.IsPlaying));

            _handles.Clear();
        }

        bool DetectMatches()
        {
            _lineRecognizer.Run(_map, _matchedLines);

            return _matchedLines.Count > 0;
        }

        bool DetectMoves()
        {
            _movementPredictor.Run(_map, _foundMoves);

            return _foundMoves.Count() > 0;
        }

        void Shuffle(bool moveGaps)
        {
            if (moveGaps) _shuffler.CanMovePredicate = x => true;

            _shuffler.Run(_map);

            if (moveGaps) _shuffler.CanMovePredicate = x => !x.IsGap;
        }
    }
}
