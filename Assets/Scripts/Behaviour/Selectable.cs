﻿using UnityEngine;
using System;

namespace WildMatch.Behaviour
{
    public sealed class Selectable : MonoBehaviour, ISelectable
    {
        public event Action<ISelectable> Selected;
        public event Action<ISelectable> Deselected;

        bool ISelectable.IsSelected
        {
            get => _selected;
            set
            {
                if (_selected == value) return;

                _selected = value;

                if (_selected)
                {
                    Selected?.Invoke(this);
                }
                else
                {
                    Deselected?.Invoke(this);
                }
            }
        }

        bool _selected;
    }
}
