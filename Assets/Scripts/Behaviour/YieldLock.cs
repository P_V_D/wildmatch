﻿using System.Collections;
using UnityEngine;
using System;

namespace WildMatch.Behaviour
{
    public sealed class YieldLock
    {
        Coroutine _coroutine;
        MonoBehaviour _owner;

        public YieldLock(MonoBehaviour owner) => _owner = owner;

        public bool StartCoroutine(Func<IEnumerator> routineCall)
        {
            if (_coroutine != null) return false;

            var wrapped = Wrap(routineCall);

            _coroutine = _owner.StartCoroutine(wrapped);

            return true;
        }

        IEnumerator Wrap(Func<IEnumerator> call)
        {
            yield return null;

            yield return call();

            _coroutine = null;
        }

        public void StopCoroutine()
        {
            if (_coroutine != null)
            {
                _owner.StopCoroutine(_coroutine);
                _coroutine = null;
            }
        }
    }
}
