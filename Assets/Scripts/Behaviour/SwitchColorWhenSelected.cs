﻿using WildMatch.Animation;
using UnityEngine;

namespace WildMatch.Behaviour
{
    public sealed class SwitchColorWhenSelected : MonoBehaviour
    {
        [SerializeField] Color _normal   = Color.white;
        [SerializeField] Color _selected = Color.white;

        ISelectable _selectable;
        IColorAnimator _animator;

        void OnEnable()
        {
            _selectable = GetComponent<ISelectable>();
            _animator = GetComponent<IColorAnimator>();

            _selectable.Selected   += OnSelected;
            _selectable.Deselected += OnDeselected;
        }

        void OnDisable()
        {
            _selectable.Selected   -= OnSelected;
            _selectable.Deselected -= OnDeselected;
        }

        void OnSelected(ISelectable sender)
        {
            _animator.AnimateColor(_selected);
        }

        void OnDeselected(ISelectable sender)
        {
            _animator.AnimateColor(_normal);
        }
    }
}
