﻿using System.Collections.Generic;
using WildMatch.Scriptable;
using System.Linq;
using UnityEngine;

namespace WildMatch.Behaviour
{
    public sealed class SelectionModifier : MonoBehaviour
    {
        // TODO: Should be interface.
        [SerializeField] CellSpawner _spawner = null;

        List<ICellPrefab> _selection = new List<ICellPrefab>();

        void Awake()
        {
            _spawner.Spawned += OnSpawned;
            _spawner.Despawned += OnDespawned;
        }

        void OnEnable()
        {
            ProcessSelectedCells();
        }

        void OnSpawned(ICellPrefab prefab)
        {
            prefab.Selected += (x) => OnSelected(prefab);
            prefab.Deselected += (x) => OnDeselected(prefab);
        }

        void OnDespawned(ICellPrefab prefab)
        {
            _selection.Remove(prefab);
        }

        void OnSelected(ICellPrefab prefab)
        {
            _selection.Add(prefab);

            ProcessSelectedCells();
        }

        void OnDeselected(ICellPrefab prefab)
        {
            _selection.Remove(prefab);
        }

        void ProcessSelectedCells()
        {
            if (!enabled) return;

            if (_selection.Count < 2) return;

            if (_selection.Count > 2)
            {
                RemoveSelection();
                return;
            }

            var a = _selection[0];
            var b = _selection[1];

            RemoveSelection();

            GameEvents.TriggerEvent(EventName.TwoCellsSelected, this, (a, b));
        }

        void RemoveSelection()
        {
            while (_selection.Count() > 0)
                _selection.Last().IsSelected = false;
        }
    }
}
