﻿using UnityEngine;

namespace WildMatch.Behaviour
{
    public sealed class GameGrid : MonoBehaviour, IGameGrid
    {
        [SerializeField][Min(0.1f)] float _cellSize = 1;
        [SerializeField][Min(1)] int _width  = 6;
        [SerializeField][Min(1)] int _height = 6;

        int IGameGrid.Width => _width;

        int IGameGrid.Height => _height;

        Vector3 IGameGrid.Transform(int x, int y)
        {
            var i = (x + (1 - _width ) * .5f) * _cellSize;
            var j = (y + (1 - _height) * .5f) * _cellSize;

            return new Vector3(i, j, 0);
        }
    }
}
