﻿using System.Collections.Generic;
using System;

namespace WildMatch.Algorythmics
{
    public struct StraightLineRecognizer2D<T>
    {
        public Func<T, T, bool> ContinueLinePredicate { get; set; }
        public int MinimumLineSize { get; set; }

        public void Run(T[,] map, IList<Int2Line> result)
        {
            if (map == null) throw new ArgumentNullException("grid was null");
            if (result == null) throw new ArgumentNullException("result was null");
            if (ContinueLinePredicate == null) throw new InvalidOperationException($"{nameof(ContinueLinePredicate)} was not assigned");
            if (MinimumLineSize < 2) throw new InvalidOperationException($"{nameof(MinimumLineSize)} < 2");

            result.Clear();
            LinesI(map, result);
            LinesJ(map, result);
        }

        void LinesI(T[,] map, IList<Int2Line> res)
        {
            var dim0 = map.GetLength(0);
            var dim1 = map.GetLength(1);
            var line = ContinueLinePredicate;

            for (var i = 0; i < dim0; i++)
            {
                for (var j = 0; j <= dim1 - MinimumLineSize;)
                {
                    var q = j + 1;

                    for(; q < dim1; q++)
                    {
                        var a = map[i, q];
                        var b = map[i, q - 1];

                        if (!line(a, b)) break;
                    }

                    if (q - j >= MinimumLineSize)
                    {
                        var a = new Int2(i, j);
                        var b = new Int2(i, q - 1);

                        res.Add(new Int2Line(a, b));
                    }

                    j = q;
                }
            }
        }

        void LinesJ(T[,] map, IList<Int2Line> res)
        {
            var dim0 = map.GetLength(0);
            var dim1 = map.GetLength(1); 
            var line = ContinueLinePredicate;

            for (var j = 0; j < dim1; j++)
            {
                for (var i = 0; i <= dim0 - MinimumLineSize;)
                {
                    var q = i + 1;

                    for (; q < dim0; q++)
                    {
                        var a = map[q, j];
                        var b = map[q - 1, j];

                        if (!line(a, b)) break;
                    }

                    if (q - i >= MinimumLineSize)
                    {
                        var a = new Int2(i, j);
                        var b = new Int2(q - 1, j);

                        res.Add(new Int2Line(a, b));
                    }

                    i = q;
                }
            }
        }
    }
}
