﻿using System.Collections.Generic;
using System;

namespace WildMatch.Algorythmics
{
    public struct StraightLinePosibilityPredictor2D<T>
    {
        public Func<T, T, bool> ContinueLinePredicate { get; set; }
        public Func<T, bool> CanMovePredicate { get; set; }
        public int MinimumLineSize { get; set; }

        public void Run(T[,] map, IList<Int2Line> result)
        {
            if (map == null) throw new ArgumentNullException("grid was null");
            if (result == null) throw new ArgumentNullException("result was null");
            if (ContinueLinePredicate == null) throw new InvalidOperationException($"{nameof(ContinueLinePredicate)} was not assigned");
            if (CanMovePredicate == null) throw new InvalidOperationException($"{nameof(CanMovePredicate)} was not assigned");
            if (MinimumLineSize < 2) throw new InvalidOperationException($"{nameof(MinimumLineSize)} < 2");

            result.Clear();
            MovementsI(map, result);
            MovementsJ(map, result);            
        }

        void MovementsI(T[,] map, IList<Int2Line> res)
        {
            var d0 = map.GetLength(0);
            var d1 = map.GetLength(1);
            var line = ContinueLinePredicate;
            var move = CanMovePredicate;

            for (var i = 0; i < d0; i++)
            {
                for (var j = 0; j <= d1 - MinimumLineSize + 1;)
                {
                    var q = j + 1;

                    for(; q < d1; q++)
                    {
                        var a = map[i, q];
                        var b = map[i, q - 1];

                        if (!line(a, b)) break;
                    }

                    if (q - j + 1 >= MinimumLineSize)
                    {
                        if (q - j >= MinimumLineSize) throw new InvalidOperationException("Map already contains a line with desired length. Results will be moot.");

                        var p = j - 1;

                        //   b   e
                        // a P---Q d 
                        //   c   f 
                        Test(i, p + 1, i, p - 1, i, p); // a
                        Test(i, p + 1, i - 1, p, i, p); // b
                        Test(i, p + 1, i + 1, p, i, p); // c
                        Test(i, q - 1, i, q + 1, i, q); // d
                        Test(i, q - 1, i - 1, q, i, q); // e
                        Test(i, q - 1, i + 1, q, i, q); // f
                    }

                    j = q;
                }

            }

            // origin, start, end
            void Test(int oi, int oj, int si, int sj, int ei, int ej)
            {
                if (si < 0   || sj < 0   || ei < 0   || ej < 0) return;
                if (si >= d0 || sj >= d1 || ei >= d0 || ej >= d1) return;

                if (!move(map[si, sj])) return;
                if (!move(map[ei, ej])) return;

                if (!line(map[oi, oj], map[si, sj])) return;

                res.Add((si, sj, ei, ej));
            }
        }

        void MovementsJ(T[,] map, IList<Int2Line> res)
        {
            var d0 = map.GetLength(0);
            var d1 = map.GetLength(1);
            var line = ContinueLinePredicate;
            var move = CanMovePredicate;

            for (var j = 0; j < d1; j++)
            {
                for (var i = 0; i <= d0 - MinimumLineSize + 1;)
                {
                    var q = i + 1;

                    for (; q < d0; q++)
                    {
                        var a = map[q, j];
                        var b = map[q - 1, j];

                        if (!line(a, b)) break;
                    }

                    if (q - i + 1 >= MinimumLineSize)
                    {
                        var p = i - 1;

                        //   b   e
                        // a P---Q d 
                        //   c   f 
                        Test(p + 1, j, p - 1, j, p, j); // a
                        Test(p + 1, j, p, j - 1, p, j); // b
                        Test(p + 1, j, p, j + 1, p, j); // c
                        Test(q - 1, j, q + 1, j, q, j); // d
                        Test(q - 1, j, q, j - 1, q, j); // e
                        Test(q - 1, j, q, j + 1, q, j); // f
                    }

                    i = q;
                }
            }

            // origin, start, end
            void Test(int oi, int oj, int si, int sj, int ei, int ej)
            {
                if (si < 0 || sj < 0 || ei < 0 || ej < 0) return;
                if (si >= d0 || sj >= d1 || ei >= d0 || ej >= d1) return;

                if (!move(map[si, sj])) return;
                if (!move(map[ei, ej])) return;

                if (!line(map[oi, oj], map[si, sj])) return;

                res.Add((si, sj, ei, ej));
            }
        }
    }
}
