﻿using System.Collections.Generic;
using System;

namespace WildMatch.Algorythmics
{
    public struct SelectiveShift2D<T>
    {
        public Func<T, bool> CanMovePredicate { get; set; }

        /// <summary>
        /// 101 -> 110 <para/>
        /// 101 -> 110 <para/>
        /// 101 -> 110 <para/>
        /// </summary>
        public void LastToFirstDim0(T[,] map)
        {
            if (map == null) throw new ArgumentNullException("map was null");
            if (CanMovePredicate == null) throw new InvalidOperationException($"{nameof(CanMovePredicate)} was not assigned");

            var dim0 = map.GetLength(0);
            var dim1 = map.GetLength(1);

            for (var i = 0; i < dim0; i++)
                for (var j = 0; j < dim1 - 1; j++)
                {
                    if (!Equals(map[i, j], default(T))) continue;

                    for (var q = j + 1; q < dim1; q++)
                    {
                        var e = map[i, q];

                        if (Equals(e, default(T)) || !CanMovePredicate(e)) continue;

                        map[i, j] = e;

                        map[i, q] = default;

                        break;
                    }
                }
        }

        /// <summary>
        /// 101 -> 011 <para/>
        /// 101 -> 011 <para/>
        /// 101 -> 011 <para/>
        /// </summary>
        public void FirstToLastDim0(T[,] map)
        {
            if (map == null) throw new ArgumentNullException("map was null");
            if (CanMovePredicate == null) throw new InvalidOperationException($"{nameof(CanMovePredicate)} was not assigned");

            var dim0 = map.GetLength(0);
            var dim1 = map.GetLength(1);

            for (var i = 0; i < dim0; i++)
                for (var j = dim1 - 1; j > 0; j--)
                {
                    if (!Equals(map[i, j], default(T))) continue;

                    for (var q = j - 1; q >= 0; q--)
                    {
                        var e = map[i, q];

                        if (Equals(e, default(T)) || !CanMovePredicate(e)) continue;

                        map[i, j] = e;

                        map[i, q] = default;

                        break;
                    }
                }
        }

        /// <summary>
        /// 111 -> 111 <para/>
        /// 000 -> 111 <para/>
        /// 111 -> 000 <para/>
        /// </summary>
        public void LastToFirstDim1(T[,] map)
        {
            if (map == null) throw new ArgumentNullException("map was null");
            if (CanMovePredicate == null) throw new InvalidOperationException($"{nameof(CanMovePredicate)} was not assigned");

            var dim0 = map.GetLength(0);
            var dim1 = map.GetLength(1);

            for (var j = 0; j < dim1; j++)
                for(var i = 0; i < dim0 - 1; i++)
                {
                    if (!Equals(map[i, j], default(T))) continue;

                    for (var q = i + 1; q < dim0; q++)
                    {
                        var e = map[q, j];

                        if (Equals(e, default(T)) || !CanMovePredicate(e)) continue;

                        map[i, j] = e;

                        map[q, j] = default;

                        break;
                    }
                }
        }

        /// <summary>
        /// 111 -> 000 <para/>
        /// 000 -> 111 <para/>
        /// 111 -> 111 <para/>
        /// </summary>
        public void FirstToLastDim1(T[,] map)
        {
            if (map == null) throw new ArgumentNullException("map was null");
            if (CanMovePredicate == null) throw new InvalidOperationException($"{nameof(CanMovePredicate)} was not assigned");

            var dim0 = map.GetLength(0);
            var dim1 = map.GetLength(1);

            for (var j = 0; j < dim1; j++)
                for(var i = dim0 - 1; i > 0; i--)
                {
                    if (!Equals(map[i, j], default(T))) continue;

                    for (var q = i - 1; q >= 0; q--)
                    {
                        var e = map[q, j];

                        if (Equals(e, default(T)) || !CanMovePredicate(e)) continue;

                        map[i, j] = e;

                        map[q, j] = default;

                        break;
                    }
                }
        }
    }
}
