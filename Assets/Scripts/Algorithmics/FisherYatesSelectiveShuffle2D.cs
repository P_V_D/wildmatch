﻿using System.Linq;
using System;

namespace WildMatch.Algorythmics
{
    public struct FisherYatesSelectiveShuffle2D<T>
    {
        public Predicate<T> CanMovePredicate { get; set; }

        public void Run(T[,] map)
        {
            if (map == null) throw new ArgumentNullException("grid was null");

            var canMove = CanMovePredicate ?? throw new InvalidOperationException($"{nameof(CanMovePredicate)} was not assigned");

            var dim1 = map.GetLength(1);

            var gaps = map.Cast<T>().Count(x => !canMove(x));

            var random = new Random();

            for (var index = map.Length - 1; index > 0; index--)
            {
                if (gaps >= index) break;

                var i = index / dim1;
                var j = index % dim1;

                if (!canMove(map[i, j]))
                {
                    gaps--;
                    continue;
                }

                // UNDONE: When gapes are dominating, it will take too much time to pick a new random item.
                int newIndex, iNew, jNew;
                do
                {
                    newIndex = random.Next(index);
                    iNew = newIndex / dim1;
                    jNew = newIndex % dim1;

                } while (!canMove(map[iNew, jNew]));

                var temp = map[i, j];
                map[i, j] = map[iNew, jNew];
                map[iNew, jNew] = temp;
            }
        }
    }
}
