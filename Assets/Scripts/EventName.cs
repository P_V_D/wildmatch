﻿namespace WildMatch
{
    public static class EventName
    {
        public static string TwoCellsSelected { get; } = nameof(TwoCellsSelected);

        public static string MatchesFound { get; } = nameof(MatchesFound);

        public static string ScoreChanged { get; } = nameof(ScoreChanged);

        public static string RerollStarted { get; } = nameof(RerollStarted);

        public static string RerollFinished { get; } = nameof(RerollFinished);

        public static string GameRoutineStarted { get; } = nameof(GameRoutineStarted);

        public static string GameRoutineFinished { get; } = nameof(GameRoutineFinished);
    }
}
