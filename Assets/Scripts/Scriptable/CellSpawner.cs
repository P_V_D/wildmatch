﻿using System.Collections.Generic;
using WildMatch.Animation;
using UnityEngine;
using System;

using URandom = UnityEngine.Random;

namespace WildMatch.Scriptable
{
    [CreateAssetMenu(fileName = nameof(CellSpawner), menuName = "ScriptableObject/" + nameof(CellSpawner), order = 1)]
    public sealed class CellSpawner : ScriptableObject
    {
        private sealed class Prefab : ICellPrefab
        {
            public bool IsSelected
            {
                get => _selectable.IsSelected;
                set => _selectable.IsSelected = value;
            }

            public event Action<ISelectable> Selected
            {
                add => _selectable.Selected += value;
                remove => _selectable.Selected -= value;
            }

            public event Action<ISelectable> Deselected
            {
                add => _selectable.Deselected += value;
                remove => _selectable.Deselected -= value;
            }

            public bool IsGap   { get; }
            public int  MatchID { get; }
            public IMotionAnimator Motion { get; }

            private readonly ISelectable _selectable;
            private readonly GameObject _source;

            public Prefab(GameObject source, int matchID, bool isGap)
            {
                _source = source;

                IsGap = isGap;
                MatchID = matchID;

                Motion = GetComponent<IMotionAnimator>(source);
                _selectable = GetComponent<ISelectable>(source);
            }

            private T GetComponent<T>(GameObject source) where T : class
            {
                return source.GetComponent<T>() ?? throw new InvalidProgramException("Desired component not found");
            }

            public void Destroy() => GameObject.Destroy(_source);
        }

        public event Action<ICellPrefab> Spawned;
        public event Action<ICellPrefab> Despawned;

        [SerializeField] List<GameObject> _commonPrefabs = new List<GameObject>();
        [SerializeField] List<GameObject> _gapPrefabs = new List<GameObject>();

        List<GameObject> _next = new List<GameObject>();

        public ICellPrefab Spawn(CellSpawnContext context)
        {
            GameObject obj = null;
            var matchID = 0;
            var isGap = false;

            switch (context)
            {
                case CellSpawnContext.CommonCell:

                    if (_next.Count == 0) _next.AddRange(_commonPrefabs);

                    var x = URandom.Range(0, _next.Count);

                    matchID = _commonPrefabs.IndexOf(_next[x]);

                    obj = Instantiate(_next[x]);

                    _next.RemoveAt(x);

                    break;

                case CellSpawnContext.GapCell:

                    isGap = true;

                    var y = URandom.Range(0, _gapPrefabs.Count);

                    obj = Instantiate(_gapPrefabs[y]);

                    break;

                default: throw new ArgumentException("invalid context");
            }

            var prefab = new Prefab(obj, matchID, isGap);

            Spawned?.Invoke(prefab);

            return prefab;
        }

        public void Despawn(ICellPrefab obj)
        {
            var prefab = obj as Prefab;

            if (prefab == null) throw new InvalidOperationException("obj is not prefab");

            prefab.Destroy();

            Despawned?.Invoke(prefab);
        }
    }
}
