﻿using UnityEngine.UI;
using UnityEngine;

namespace WildMatch.UI
{
    public sealed class TextAnimator : MonoBehaviour
    {
        [SerializeField] string[] _lines = null;
        [SerializeField] Text _text = null;
        [SerializeField] float _delay = .5f;

        float _timer;
        int _current;

        void Update()
        {
            _timer += Time.deltaTime;

            if (_timer < _delay) return;

            _current = (_current + 1) % _lines.Length;

            _text.text = _lines[_current];

            _timer -= _delay;
        }
    }
}
