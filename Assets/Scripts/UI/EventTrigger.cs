﻿using UnityEngine.Events;
using UnityEngine;

namespace WildMatch.Behaviour
{
    public sealed class EventTrigger : MonoBehaviour
    {
        [SerializeField] string _eventName = "";
        [SerializeField] UnityEvent _onEvent = null;

        private void OnEnable()
        {
            GameEvents.AddListener(_eventName, OnEventTriggered);
        }

        private void OnEventTriggered(object sender, object args)
        {
            if (!enabled) return;

            _onEvent.Invoke();
        }
    }
}
