﻿using UnityEngine.UI;
using UnityEngine;

namespace WildMatch.UI
{
    public sealed class ScoreDisplay : MonoBehaviour
    {
        [SerializeField] Text _text = null;

        void OnEnable()
        {
            GameEvents.AddListener(EventName.ScoreChanged, OnScoreChanged);
        }

        void OnScoreChanged(object sender, object eventArgs)
        {
            if (!enabled) return;

            var score = (int)eventArgs;
            _text.text = score.ToString();
        }
    }
}
