﻿namespace WildMatch
{
    public struct Int2
    {
        public int x;
        public int y;

        public static implicit operator Int2((int i, int j) source) => new Int2(source.i, source.j);

        public Int2(int i, int j)
        {
            this.x = i;
            this.y = j;
        }

        public override string ToString() => "(" + x + ", " + y + ")";

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (GetType() != obj.GetType()) return false;

            return this == (Int2)obj;
        }

        public override int GetHashCode() => (x << 2) ^ y;

        public static bool operator ==(Int2 a, Int2 b) => (a.x == b.x) && (a.y == b.y);

        public static bool operator !=(Int2 a, Int2 b) => (a.x != b.x) || (a.y != b.y);
    }
}
