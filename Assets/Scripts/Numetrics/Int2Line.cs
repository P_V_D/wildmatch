﻿using System.Collections.Generic;
using System;

namespace WildMatch
{
    public struct Int2Line
    {
        public Int2 a;
        public Int2 b;

        public int PointCount
        {
            get
            {
                if (a.x == b.x) return Math.Abs(a.y - b.y) + 1;
                if (a.y == b.y) return Math.Abs(a.x - b.x) + 1;

                throw new NotSupportedException("Only straight lines are supported");
            }
        }

        public IEnumerable<Int2> Points
        {
            get
            {
                if (a.x == b.x)
                {
                    var min = Math.Min(a.y, b.y);
                    var max = Math.Max(a.y, b.y);

                    for (var i = min; i <= max; i++)
                        yield return new Int2(a.x, i);

                    yield break;
                }

                if (a.y == b.y)
                {
                    var min = Math.Min(a.x, b.x);
                    var max = Math.Max(a.x, b.x);

                    for (var i = min; i <= max; i++)
                        yield return new Int2(i, a.y);

                    yield break;
                }

                throw new NotSupportedException("Only straight lines are supported");
            }
        }

        public override string ToString() => a.ToString() + ":" + b.ToString();

        public static implicit operator Int2Line((int ai, int aj, int bi, int bj) source) => new Int2Line(source.ai, source.aj, source.bi, source.bj);

        public Int2Line(Int2 a, Int2 b)
        {
            this.a = a;
            this.b = b;
        }

        public Int2Line(int ai, int aj, int bi, int bj)
        {
            a.x = ai;
            a.y = aj;
            b.x = bi;
            b.y = bj;
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (GetType() != obj.GetType()) return false;

            return this == (Int2Line)obj;
        }

        public override int GetHashCode()
        {
            var i = (a.x << 2) ^ a.y;
            var j = (b.y << 2) ^ b.x;

            return i + j;
        }

        public static bool operator ==(Int2Line line0, Int2Line line1)
        {
            if ((line0.a == line1.a) && (line0.b == line1.b)) return true;
            if ((line0.a == line1.b) && (line0.b == line1.a)) return true;

            return false;
        }

        public static bool operator !=(Int2Line line0, Int2Line line1)
        {
            if ((line0.a == line1.a) && (line0.b == line1.b)) return false;
            if ((line0.a == line1.b) && (line0.b == line1.a)) return false;

            return true;
        }
    }
}
