﻿using UnityEngine;

namespace WildMatch.Animation
{
    public interface IColorAnimator
    {
        IAnimationHandle AnimateColor(Color start, Color final);

        IAnimationHandle AnimateColor(Color desired);
    }
}
