﻿using System;

namespace WildMatch
{
    public interface ISelectable
    {
        event Action<ISelectable> Selected;
        event Action<ISelectable> Deselected;

        bool IsSelected { get; set; }
    }
}
