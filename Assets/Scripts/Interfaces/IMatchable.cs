﻿namespace WildMatch
{
    public interface IMatchable
    {
        bool IsGap { get; }

        int MatchID { get; }
    }
}
