﻿namespace WildMatch.Animation
{
    public interface IAnimationHandle
    {
        bool IsPlaying { get; }

        void Finish();
    }
}
