﻿using UnityEngine;

namespace WildMatch
{
    public interface IGameGrid
    {
        int Width  { get; }
        int Height { get; }

        Vector3 Transform(int x, int y);
    }
}
