﻿using WildMatch.Animation;

namespace WildMatch
{
    public interface ICellPrefab : ISelectable, IMatchable
    {
        IMotionAnimator Motion { get; }
    }
}
