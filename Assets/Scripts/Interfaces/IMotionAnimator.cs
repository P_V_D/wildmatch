﻿using UnityEngine;

namespace WildMatch.Animation
{
    public interface IMotionAnimator
    {
        IAnimationHandle AnimateMovement(Vector3 desiredPosition);

        IAnimationHandle AnimateMovement(Vector3 startPosition, Vector3 finalPosition);
    }
}
