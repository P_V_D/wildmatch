﻿using WildMatch.Algorythmics;
using NUnit.Framework;
using System.Linq;
using WildMatch;
using System;

namespace Tests
{
    public sealed class FisherYatesSelectiveShuffle2DTest
    {
        class IntWrapper
        {
            public int Value { get; set; }
        }

        [Test]
        public void Test()
        {
            var random = new Random();

            for (var test = 0; test < 500; test++)
            {
                var size = (random.Next(32) + 1, random.Next(32) + 1);
                var grid = CreateRandomizedArray(size, 10);
                var gridClone = grid.Clone() as IntWrapper[,];

                var shuffle = new FisherYatesSelectiveShuffle2D<IntWrapper>()
                {
                    CanMovePredicate = x => x.Value != 0
                };

                shuffle.Run(gridClone);

                // Test references are same.
                var expectedCells = grid.Cast<IntWrapper>();
                var actualCells = gridClone.Cast<IntWrapper>();
                Assert.AreEqual(grid.Length, expectedCells.Intersect(actualCells).Count());

                // Test any movable element has been moved. And non-movable has not.
                var movable = expectedCells.Count(x => x.Value != 0);
                if (movable > 1)
                {
                    foreach(var (expected, actual) in expectedCells.Zip(actualCells, Tuple.Create))
                    {
                        if (expected.Value == 0)
                        {
                            Assert.AreSame(expected, actual);
                        }
                        else
                        {
                            Assert.AreNotSame(expected, actual);
                        }
                    }
                }              
            }
        }

        IntWrapper[,] CreateRandomizedArray(Int2 size, int maxValue)
        {
            var random = new Random();

            var result = new IntWrapper[size.x, size.y];
            for (var i = 0; i < size.x; i++)
                for (var j = 0; j < size.y; j++)
                    result[i, j] = new IntWrapper()
                    {
                        Value = random.Next(maxValue)
                    };

            return result;
        }
    }
}
