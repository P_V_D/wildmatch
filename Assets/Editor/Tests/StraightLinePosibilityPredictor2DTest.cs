﻿using System.Collections.Generic;
using WildMatch.Algorythmics;
using NUnit.Framework;
using System.Linq;
using WildMatch;

namespace Tests
{
    sealed class StraightLinePosibilityPredictor2DTest
    {
        StraightLinePosibilityPredictor2D<int> _predictor;
        List<Int2Line> _result;

        public StraightLinePosibilityPredictor2DTest()
        {
            _predictor = new StraightLinePosibilityPredictor2D<int>()
            {
                MinimumLineSize = 3,
                CanMovePredicate = x => x != 9,
                ContinueLinePredicate = (a, b) =>
                {
                    if (a == 9) return false;
                    if (b == 9) return false;
                    return a == b;
                }
            };

            _result = new List<Int2Line>();
        }

        [Test]
        public void TestA()
        {
            int[,] map = { { 1, 0, 1, 1} };

            Int2Line[] expected = { (0, 0, 0, 1) };

            _predictor.Run(map, _result);
            Validate(expected);
        }

        [Test]
        public void TestB()
        {
            int[,] map = { { 1 }, { 0 }, { 1 }, { 1 } };

            Int2Line[] expected = { (0, 0, 1, 0) };

            _predictor.Run(map, _result);
            Validate(expected);
        }

        [Test]
        public void TestC()
        {
            int[,] map =
            {
                { 9, 1, 9, 9, 1, 9 },
                { 1, 0, 1, 1, 0, 1 },
                { 9, 1, 9, 9, 1, 9 },
            };

            Int2Line[] expected =
            {
                (0, 1, 1, 1),
                (1, 0, 1, 1),
                (2, 1, 1, 1),
                (0, 4, 1, 4),
                (1, 5, 1, 4),
                (2, 4, 1, 4),
            };

            _predictor.Run(map, _result);
            Validate(expected);
        }

        [Test]
        public void TestD()
        {
            int[,] map =
            {
                { 0, 1, 0 },
                { 1, 0, 1 },
                { 9, 1, 9 },
                { 9, 1, 9 },
                { 1, 0, 1 },
                { 0, 1, 0 },
            };

            Int2Line[] expected =
            {
                (1, 0, 1, 1),
                (0, 1, 1, 1),
                (1, 2, 1, 1),
                (4, 0, 4, 1),
                (5, 1, 4, 1),
                (4, 2, 4, 1),
            };

            _predictor.Run(map, _result);
            Validate(expected);
        }

        [Test]
        public void TestE()
        {
            int[,] map =
            {
                { 9, 1, 9, 9, 1, 9 },
                { 1, 9, 1, 1, 9, 1 },
                { 9, 1, 9, 9, 1, 9 },
            };

            Int2Line[] expected = { };

            _predictor.Run(map, _result);
            Validate(expected);
        }

        [Test]
        public void TestF()
        {
            int[,] map =
            {
                { 9, 1, 9 },
                { 1, 9, 1 },
                { 9, 1, 9 },
                { 9, 1, 9 },
                { 1, 9, 1 },
                { 9, 1, 9 },
            };

            Int2Line[] expected = { };

            _predictor.Run(map, _result);
            Validate(expected);
        }

        void Validate(Int2Line[] expected)
        {
            Assert.AreEqual(expected.Length, _result.Count);

            var same = _result.Intersect(expected);

            Assert.AreEqual(expected.Length, same.Count());
        }
    }
}
