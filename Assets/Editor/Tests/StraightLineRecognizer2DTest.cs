﻿using System.Collections.Generic;
using WildMatch.Algorythmics;
using NUnit.Framework;
using System.Linq;
using WildMatch;

namespace Tests
{
    public sealed class StraightLineRecognizer2DTest
    {
        List<Int2Line> _result;
        StraightLineRecognizer2D<int> _recon;

        public StraightLineRecognizer2DTest()
        {
            _result = new List<Int2Line>();

            _recon = new StraightLineRecognizer2D<int>()
            {
                ContinueLinePredicate = (a, b) => a == b
            };
        }

        [Test]
        public void TestA()
        {
            var grid = new int[,]
            {
                { 1, 0, 2, 0, 1, 0 },
                { 0, 2, 2, 2, 0, 1 },
                { 1, 0, 2, 0, 1, 0 },
            };

            var expected = new Int2Line[]
            {
                (0, 2, 2, 2),
                (1, 1, 1, 3),
            };

            RunTest(grid, 3, expected);
        }

        [Test]
        public void TestB()
        {
            var grid = new int[,]
            {
                { 0, 0, 1, 2, 1, 0, 0 },
                { 0, 1, 2, 1, 2, 1, 0 },
                { 1, 2, 1, 2, 1, 2, 0 },
            };

            var expected = new Int2Line[]
            {
                (0, 0, 0, 1),
                (0, 0, 1, 0),
                (0, 5, 0, 6),
                (0, 6, 2, 6)
            };

            RunTest(grid, 2, expected);
        }

        [Test]
        public void TestC()
        {
            var grid = new int[,]
            {
                { 0, 0, 0, 1, 0, 0, 0,},
                { 0, 1, 0, 1, 1, 2, 1 },
                { 0, 2, 0, 2, 2, 1, 2 },
                { 0, 1, 1, 2, 1, 2, 1 },
                { 0, 2, 0, 1, 2, 1, 2 },
                { 0, 1, 0, 2, 1, 2, 1 },
                { 0, 2, 0, 1, 2, 1, 2 },
            };

            var expected = new Int2Line[]
            {
                (0, 0, 0, 2),
                (0, 0, 6, 0),
                (0, 2, 2, 2),
                (4, 2, 6, 2),
                (0, 4, 0, 6),
            };

            RunTest(grid, 3, expected);

        }

        void RunTest(int[,] grid, int lineSize, Int2Line[] expected)
        {
            _recon.MinimumLineSize = lineSize;
            _recon.Run(grid, _result);

            Assert.IsTrue(_result.Count() == expected.Length);
            Assert.IsTrue(expected.Intersect(_result).Count() == expected.Length);
        }
    }
}
