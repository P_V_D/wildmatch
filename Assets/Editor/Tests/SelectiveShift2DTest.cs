﻿using WildMatch.Algorythmics;
using NUnit.Framework;

namespace Tests
{
    public sealed class SelectiveShift2DTest
    {
        SelectiveShift2D<int> _shift;

        public SelectiveShift2DTest()
        {
            _shift = new SelectiveShift2D<int>()
            {
                CanMovePredicate = x => x != 9
            };
        }

        [Test]
        public void LastToFirstDim0()
        {
            int[,] actual =
            {
                { 1, 9, 0, 0, 0, 1 },
                { 0, 1, 9, 1, 0, 1 },
                { 9, 0, 9, 9, 9, 1 },
            };

            int[,] expected =
            {
                { 1, 9, 1, 0, 0, 0 },
                { 1, 1, 9, 1, 0, 0 },
                { 9, 1, 9, 9, 9, 0 },
            };

            _shift.LastToFirstDim0(actual);

            Validate(actual, expected);
        }

        [Test]
        public void FirstToLastDim0()
        {
            int[,] actual =
            {
                { 0, 1, 9, 0, 1, 0 },
                { 9, 1, 9, 9, 9, 0 },
                { 1, 0, 1, 9, 0, 0 },
            };

            int[,] expected =
            {
                { 0, 0, 9, 0, 1, 1 },
                { 9, 0, 9, 9, 9, 1 },
                { 0, 0, 0, 9, 1, 1 },
            };

            _shift.FirstToLastDim0(actual);

            Validate(actual, expected);
        }

        [Test]
        public void LastToFirstDim1()
        {
            int[,] actual =
            {
                { 9, 1, 0 },
                { 9, 9, 0 },
                { 0, 0, 1 },
                { 9, 0, 9 },
                { 9, 1, 1 },
                { 1, 0, 9 },
                { 0, 0, 1 },
            };

            int[,] expected =
            {
                { 9, 1, 1 },
                { 9, 9, 1 },
                { 1, 1, 1 },
                { 9, 0, 9 },
                { 9, 0, 0 },
                { 0, 0, 9 },
                { 0, 0, 0 },
            };

            _shift.LastToFirstDim1(actual);

            Validate(actual, expected);
        }

        [Test]
        public void FirstToLastDim1()
        {
            int[,] actual =
            {
                { 1, 0, 0 },
                { 1, 1, 1 },
                { 9, 0, 9 },
                { 0, 1, 9 },
                { 1, 9, 1 },
                { 1, 9, 9 },
                { 0, 9, 0 },
            };

            int[,] expected =
            {
                { 0, 0, 0 },
                { 0, 0, 0 },
                { 9, 1, 9 },
                { 1, 1, 9 },
                { 1, 9, 1 },
                { 1, 9, 9 },
                { 1, 9, 1 },
            };

            _shift.FirstToLastDim1(actual);

            Validate(actual, expected);
        }

        void Validate(int[,] actual, int[,] expected)
        {
            for (var i = 0; i < actual.GetLength(0); i++)
                for (var j = 0; j < actual.GetLength(1); j++)
                    Assert.AreEqual(expected[i, j], actual[i, j]);
        }
    }
}
